import { SensorRepositorio } from "../persistencia/sensorRepositorio";
import { Sensor } from "../entidades/sensor";

export async function getSensor(idSensor: number){

    let statusUmidade = await SensorRepositorio.getSensor(idSensor);

    return statusUmidade;
}

export async function getTodosSensores(){

    let todosSensores = await SensorRepositorio.getTodosSensores();

    return todosSensores;
}

export async function cadastrarSensor(sensor: Sensor){

    let novoCadastro = await SensorRepositorio.cadastrarSensorPlanta(sensor);

    return novoCadastro;
}

export async function verificaUmidadePlanta(idSensor: number, novaUmidade: number){

    let statusUmidade = await SensorRepositorio.verificarUmidadePlanta(idSensor,novaUmidade);

    return statusUmidade;
}
