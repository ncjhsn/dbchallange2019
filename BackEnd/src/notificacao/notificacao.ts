import nodemailer from "nodemailer";

export function enviarEmail() {
    const email = '<REMETENTE>@gmail.com';
    const passwd = '<SENHA>';

    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: email,
            pass: passwd
        }
    });

    const mailOptions = {
        from: email,
        to: '<DESTINATARIO>@gmail.com',
        subject: 'Status da planta',
        html: '<p> Planta com Umidade fora do intervalo! </p>'
    };

    transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
            console.log(err.message);
        } else {
            console.log(info);
        }
    });
}