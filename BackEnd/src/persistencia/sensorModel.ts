import { Sensor } from "../entidades/sensor";
import { Document, model, Schema, Model } from "mongoose";

interface SensorDocument extends Sensor, Document {}

export const SensorModel: Model<SensorDocument> = model<SensorDocument>('Sensor', new Schema({
    
    id_sensor: { type: Number, required: true},
    nome_planta: {type: String, required: true, max: 50 },
    umidade_atual : { type: Number, required: true, max: 1000},
    umidade_minima : { type: Number, required: true, max: 1000},
    umidade_maxima : { type: Number, required: true, max: 1000}
}), 'sensores');