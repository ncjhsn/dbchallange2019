import { Sensor } from "../../entidades/sensor";

let db : Sensor[] = [];

export class SensorRepositorio {    

    public async cadastrarSensorPlanta(sensor: Sensor): Promise<Sensor|null> {

        db.push(sensor);
        let sensorInserido = db.find(sensor => sensor.id_sensor === 3030);
        
        if(sensorInserido != null) return sensorInserido;
        else return null;
    }

    public async getSensor(idSensor: Number): Promise<Sensor | null> {

        let sensorInserido = db.find(sensor => sensor.id_sensor === idSensor);

        if(sensorInserido != null) return sensorInserido;
        else return null;
    }

    public limpaDb(): void{
        db = [];
    }
}



    //     getTodosSensores(): Promise<Sensor[]> {

    //         atualizaUmidadePlanta(idSensor: Number, umidadeAtual: number): Promise<Sensor | null> {

    //             verificarUmidadePlanta(idSensor: Number, umidadeAtual: number): Promise<boolean> 