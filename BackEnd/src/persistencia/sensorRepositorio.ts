import { Sensor } from "../entidades/sensor";
import { SensorModel } from "./sensorModel";
import { enviarEmail } from "../notificacao/notificacao";
import { verificaUmidadePlanta } from "../negocio/negocio";


export class SensorRepositorio {

    static async cadastrarSensorPlanta(sensor: Sensor): Promise<Sensor> {

        let novoSensor = await SensorModel.create(sensor);
        return novoSensor.save();
    }

    static async getSensor(idSensor: Number): Promise<Sensor | null> {

        let sensor = await SensorModel.findOne().where('id_sensor').equals(idSensor).exec();

        if (sensor != null) {
            let status = await verificaUmidadePlanta(sensor.id_sensor, sensor.umidade_atual);
            console.log(status);
            if (!status) {
                await enviarEmail();
            }
        }
        return sensor;

    }

    static async getTodosSensores(): Promise<Sensor[]> {

        let sensores = await SensorModel.find().exec();

        return sensores;

    }

    static async atualizaUmidadePlanta(idSensor: Number, umidadeAtual: number): Promise<Sensor | null> {

        let sensor = await SensorModel.findOne().where('id_sensor').equals(idSensor).exec();

        if (sensor != null) {

            sensor.umidade_atual = umidadeAtual;
            sensor.save();
        }
        return sensor;
    }

    static async verificarUmidadePlanta(idSensor: Number, umidadeAtual: number): Promise<boolean> {

        let sensor = await SensorModel.findOne().where('id_sensor').equals(idSensor).exec();
        let status: boolean = false;

        if (sensor != null) {

            if (umidadeAtual <= sensor.umidade_maxima && umidadeAtual >= sensor.umidade_minima) {
                status = true;
            }
        }
        this.atualizaUmidadePlanta(idSensor, umidadeAtual);

        return status;
    }



}