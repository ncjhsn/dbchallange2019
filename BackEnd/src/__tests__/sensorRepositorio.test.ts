import { SensorRepositorio } from "../persistencia/__mocks__/sensorRepositorio";
import { cadastraSensor } from "../api/sensoresControlador";
import { Sensor } from "../entidades/sensor";

//jest.mock('sensorRepositorio');

let sensorRepositorio: SensorRepositorio = new SensorRepositorio();

let sensor: Sensor = {
    id_sensor: 3030,
    nome_planta: 'cogumelos',
    umidade_atual: 50,
    umidade_minima: 30,
    umidade_maxima: 80,
} 

afterEach(() => {
    sensorRepositorio.limpaDb();
  });

test('Verifica se sensor foi inserido com exito e retorna ele após inserido', async () => {
    expect(await sensorRepositorio.cadastrarSensorPlanta(sensor)).toBe(sensor);
});



test('Verifica se sensor foi ', async () => {
    await sensorRepositorio.cadastrarSensorPlanta(sensor);
    expect(await sensorRepositorio.getSensor(sensor.id_sensor)).toBe(sensor);
});