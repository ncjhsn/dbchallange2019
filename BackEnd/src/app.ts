import express from 'express';
import bodyParser from 'body-parser';
import errorhandler from 'errorhandler';
import { path as pathSensores, router  as routerSensores, router} from './api/sensoresRota';
import cors from 'cors';

const app = express();

app.use(cors());
app.set('port', process.env.port);
app.use(bodyParser.json());
app.use(pathSensores,routerSensores);
app.use('', router);
if(process.env.node_env !== "production") {
    app.use(errorhandler());
}

export default app;