import{Router} from 'express';
import * as controlador from './sensoresControlador';

const router = Router();
export const path = '/sensores';

router.post(`${path}/cadastraSensor`,controlador.cadastraSensor);

router.get(`${path}/buscaSensor/:id`, controlador.buscaSensor);

router.get(`${path}/buscaTodos/`, controlador.buscaSensores);

router.post(`${path}/verificaUmidade`,controlador.verificaUmidade);

export {router}
