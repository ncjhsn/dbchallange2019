import { Request, Response, NextFunction } from 'express';
import * as negocio from '../negocio/negocio';
import { ObjectID } from 'bson';
import { Sensor } from '../entidades/sensor';
import { SensorRepositorio } from '../persistencia/sensorRepositorio';


export async function cadastraSensor(req: Request, res: Response, next: NextFunction) {

    try {
        const sensor = req.body as Sensor;
        await SensorRepositorio.cadastrarSensorPlanta(sensor);
        res.status(201).send("Cadastro Realizado!");

    } catch (error) {
        next(error);
    }
}

export async function buscaSensor(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.params.id;
        const sensor = await negocio.getSensor(id);
        res.json(sensor);

    } catch (error) {
        next(error);
    }
}

export async function buscaSensores(req: Request, res: Response, next: NextFunction) {
    try {
        const sensores = await negocio.getTodosSensores();
        res.json(sensores);

    } catch (error) {
        next(error);
    }
}



export async function verificaUmidade(req: Request, res: Response, next: NextFunction) {
    try {
        const id_sensor = req.body.id_sensor as number;
        const umidade = req.body.umidade as number;
        await SensorRepositorio.verificarUmidadePlanta(id_sensor, umidade);
        res.status(201).send("Umidade Atulizada!");

    } catch (error) {
        next(error);
    }
}


