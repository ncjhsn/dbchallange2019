import app from './app';
import { connect } from 'mongoose';

(async () => {

  try {
    //conexão ao MongoDB

    const servidorMongoDb = 'mongodb+srv://dbUser:dbUser@cluster0-oafwk.mongodb.net/test?retryWrites=true&w=majority';

    await connect(servidorMongoDb, { useNewUrlParser: true })

    //iniciar Express

    app.listen(3000, () => {
      console.log(`Express executando em http://localhost:3000 no modo ${app.get('env')}`);
    });


  } catch (error) {

    console.log('Erro:');
    console.log(error.message);
  }
})();
