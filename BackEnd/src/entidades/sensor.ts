
export interface Sensor{

    id_sensor : number;
    nome_planta : string;
    umidade_atual : number;
    umidade_minima : number;
    umidade_maxima : number;

}