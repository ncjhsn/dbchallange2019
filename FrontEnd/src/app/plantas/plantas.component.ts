import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-plantas',
  templateUrl: './plantas.component.html',
  styleUrls: ['./plantas.component.scss']
})
export class PlantasComponent implements OnInit {

  plantas: Array<any>;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.listarPlantas();
  }

  listarPlantas() {
    this.apiService.listarPlantas().subscribe(data => this.plantas = data);
  }

}
