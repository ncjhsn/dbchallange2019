import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-planta',
  templateUrl: './planta.component.html',
  styleUrls: ['./planta.component.scss']
})
export class PlantaComponent implements OnInit {

  planta: Array<any>;
  id = 0;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.listarPlanta(this.id);
  }
  
  listarPlanta(id: number) {
    this.apiService.listarPlanta(id).subscribe(data => this.planta = data);
  }

}
