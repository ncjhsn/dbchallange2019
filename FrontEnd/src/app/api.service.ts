import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  urlPlanta = 'http://localhost:3000/sensores/buscaSensor/';
  urlPlantas = 'http://localhost:3000/sensores/buscaTodos';

  constructor(private http: HttpClient) { }

  listarPlanta(id: number) {
    return this.http.get<any>(this.urlPlanta + id);
  }

  listarPlantas() {
    return this.http.get<any[]>(this.urlPlantas);
  }

}
