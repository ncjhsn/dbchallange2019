import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlantasComponent } from './plantas/plantas.component';
import { PlantaComponent } from './planta/planta.component';


const routes: Routes = [
  {path: 'plantas', component: PlantasComponent},
  {path: 'planta', component: PlantaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
